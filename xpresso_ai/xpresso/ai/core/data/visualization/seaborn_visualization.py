"""Class for visualization of attribute metrics using seaborn, matplotlib"""

__all__ = ["SeabornVisualization"]
__author__ = ["Ashritha Goramane"]

from xpresso.ai.core.commons.utils.constants import REPORT_OUTPUT_PATH, \
    EMPTY_STRING
from xpresso.ai.core.data.automl.data_type import DataType
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.visualization.abstract_visualization import \
    PlotType
from xpresso.ai.core.data.visualization.base_visualization import \
    BaseVisualization
from xpresso.ai.core.data.visualization.report import ScatterReport
from xpresso.ai.core.data.visualization.seaborn import scatter, bar, box, pie, \
    density, heatmap, mosaic
from xpresso.ai.core.logging.xpr_log import XprLogger


class SeabornVisualization(BaseVisualization):
    """
        Visualization class takes a automl object and provide functions
        to render plots on the metrics of attribute
        Args:
            dataset(:obj StructuredDataset): Structured dataset object on
            which visualization to be performed
    """

    def __init__(self, dataset):
        super().__init__(dataset)
        self.dataset = dataset
        self.attribute_info = dataset.info.attributeInfo
        self.metric = dataset.info.metrics
        self.logger = XprLogger()
        self.pie_plot = pie
        self.quartile_plot = box
        self.bar_plot = bar
        self.heatmap_plot = heatmap
        self.density_plot = density
        self.scatter_plot = scatter
        self.mosaic_plot = mosaic

    def plot(self, field=None, attr=None, plot_type=None,
             output_format=utils.HTML, others_percent_threshold=None,
             bar_plot_tail_threshold=None):
        """
        Overrides function to render specific plot for field in metrics
        depending upon the type of plot
        Args:
            field(str): Metric field of the data to be plotted
            attr(:obj:AttributeInfo): Attribute of which plot to be generated
            plot_type (:obj: `PlotType`): Specifies the type of plot
                to be generated
            output_format (str): html, png or pdf plots to be generated
            others_percent_threshold(int): Optional threshold to club small
                categories in pie chart
            bar_plot_tail_threshold(int): Optional threshold to club tail
                in bar chart
        """
        inputs = self.process_input(field=field, attr=attr, plot_type=plot_type)
        if not inputs:
            return False
        plot_file_name, plot_title, input_1, input_2, axes_labels = inputs
        if plot_type == PlotType.QUARTILE.value:
            self.quartile_plot.NewPlot(input_1, output_format=output_format,
                                       plot_title=plot_title,
                                       file_name=plot_file_name,
                                       axes_labels=axes_labels)
            return True
        elif plot_type == PlotType.PIE.value:
            self.pie_plot.NewPlot(input_2, input_1, output_format=output_format,
                                  plot_title=plot_title,
                                  file_name=plot_file_name,
                                  threshold=others_percent_threshold)
            return True
        elif plot_type == PlotType.BAR.value:
            self.bar_plot.NewPlot(input_1, input_2, output_format=output_format,
                                  plot_title=plot_title,
                                  file_name=plot_file_name,
                                  axes_labels=axes_labels,
                                  threshold=bar_plot_tail_threshold)
            return True
        return False

    def target_variable_plot(self, target, attr_name=None,
                             output_format=utils.HTML):
        """
        Overrides helper function to plot all combination of target
        variable plots
        Args:
            target(:str): Target variable name
            attr_name(:str): optional attribute name to plot specific
            attribute vs target plots
            output_format (str): html, png or pdf plots to be generated
        """
        attr_list = list(filter(lambda attr: attr.name == target,
                                self.attribute_info))
        if not attr_list:
            self.logger.error("{} attribute doesn't exist".format(target))
            return
        target_attr = attr_list[0]

        for attr in self.attribute_info:
            if ((attr.name != attr_name and attr_name is not None) or (
                    self.dataset.data[
                        attr.name].isnull().all()) or attr.name == target):
                continue
            if target_attr.type in [DataType.NOMINAL.value,
                                    DataType.ORDINAL.value] \
                    and attr.type == DataType.NUMERIC.value:
                self.plot_target(attr, target_attr, PlotType.QUARTILE.value,
                                 output_format)
                self.plot_target(attr, target_attr, PlotType.DENSITY.value,
                                 output_format)
            elif target_attr.type in [DataType.NOMINAL.value,
                                      DataType.ORDINAL.value] \
                    and attr.type in [DataType.NOMINAL.value,
                                      DataType.ORDINAL.value]:
                self.plot_target(attr, target_attr, PlotType.BAR.value,
                                 output_format)
                self.plot_target(attr, target_attr, PlotType.MOSAIC.value,
                                 output_format)
            elif target_attr.type == DataType.NUMERIC.value \
                    and attr.type == DataType.NUMERIC.value:
                self.plot_target(attr, target_attr, PlotType.SCATTER.value,
                                 output_format)
            elif target_attr.type == DataType.NUMERIC.value \
                    and attr.type in [DataType.NOMINAL.value,
                                      DataType.ORDINAL.value]:
                self.plot_target(attr, target_attr, PlotType.QUARTILE.value,
                                 output_format)

    def plot_target(self, attribute, target, plot_type, output_format=None):
        """
        Helper function to plot specific target variable plot
        Args:
            target(:str): Target variable name
            attribute(:obj attribute_info): optional attribute to plot specific
                attribute vs target plots
            plot_type(:str): Specifies the type of plot
                to be generated
            output_format (str): html, png or pdf plots to be generated
        """
        input_1 = self.dataset.data[attribute.name]
        input_2 = self.dataset.data[target.name]
        plot_file_name = "{}_target".format(attribute.name, plot_type)
        plot_title = "{} plot for {}".format(plot_type,
                                             attribute.name).capitalize()

        if plot_type == PlotType.BAR.value:
            axes_labels = self.set_axes_labels(attribute.name,
                                               "Count({})".format(
                                                   attribute.name), target.name)
            self.bar_plot.NewPlot(input_1, target=input_2,
                                  plot_title=plot_title,
                                  file_name=plot_file_name,
                                  output_format=output_format,
                                  axes_labels=axes_labels)
        elif plot_type == PlotType.SCATTER.value:
            axes_labels = self.set_axes_labels(attribute.name, target.name,
                                               target.name)
            self.scatter_plot.NewPlot(input_1, input_2, target=True,
                                      auto_render=True, plot_title=plot_title,
                                      file_name=plot_file_name,
                                      output_format=output_format,
                                      axes_labels=axes_labels)
        elif plot_type == PlotType.QUARTILE.value:
            axes_labels = self.set_axes_labels(target.name,
                                               EMPTY_STRING,
                                               EMPTY_STRING)
            self.quartile_plot.NewPlot(input_1, target=input_2,
                                       plot_title=plot_title,
                                       file_name=plot_file_name,
                                       output_format=output_format,
                                       axes_labels=axes_labels)
        elif plot_type == PlotType.DENSITY.value:
            axes_labels = self.set_axes_labels(attribute.name,
                                               "P({})".format(attribute.name),
                                               target.name)
            self.density_plot.NewPlot(input_1, target=input_2,
                                      plot_title=plot_title,
                                      file_name=plot_file_name,
                                      output_format=output_format,
                                      axes_labels=axes_labels)
        elif plot_type == PlotType.MOSAIC.value:
            axes_labels = self.set_axes_labels(attribute.name, target.name)
            self.mosaic_plot.NewPlot(input_1, input_2, plot_title=plot_title,
                                     file_name=plot_file_name,
                                     output_format=output_format,
                                     axes_labels=axes_labels)

    def plot_multivariate(self, corr_df, corr, output_format=utils.HTML):
        """
        Overrides helper function to plot heatmap
        Args:
            corr_df(DataFrame): Dataframe of correlation matrix
            corr(str): correlation coefficient
            output_format(str): output format for the plot (png/html)
        """
        self.heatmap_plot.NewPlot(corr_df,
                                  output_format=output_format,
                                  plot_title=corr,
                                  file_name=corr)
        return True

    def scatter(self, attribute_x=None, attribute_y=None,
                output_format=utils.HTML, file_name=None,
                output_path=REPORT_OUTPUT_PATH, report=False, target=False):
        """
        Renders scatter plots for the numeric attributes
        Args:
            attribute_x(str): x axis attribute
            attribute_y(str): y axis attribute
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the html/pdf/png plots to be stored
            file_name(str): File name of the plot to be stored
            report (bool): if true generates a report
            target(str): Name of target variable in dataset
        """
        inputs = self.process_scatter_input(attribute_x, attribute_y,
                                            output_format, report)
        if not inputs:
            return False
        output_format, numeric_attr = inputs

        if attribute_x is not None and attribute_y is not None:
            axes_labels = self.set_axes_labels(attribute_x, attribute_y)
            plot_file_name = "{}_scatter".format(attribute_x)
            plot_title = "Scatter plot for {} {}".format(attribute_x,
                                                         attribute_y)
            self.scatter_plot.NewPlot(self.dataset.data[attribute_x],
                                      self.dataset.data[attribute_y],
                                      axes_labels=axes_labels, auto_render=True,
                                      output_format=output_format,
                                      file_name=plot_file_name,
                                      plot_title=plot_title, target=target)
        elif attribute_x is not None:
            plot_file_name = "{}_scatter".format(attribute_x)
            plot_title = "Scatter plot for {}".format(attribute_x)
            attribute_y = list(set(numeric_attr) - {attribute_x})
            self.scatter_plot.Join(self.dataset.data[attribute_x],
                                   self.dataset.data[attribute_y],
                                   output_format=output_format,
                                   file_name=plot_file_name,
                                   plot_title=plot_title, target=target)
        elif attribute_x is None and attribute_y is None:
            for attribute_x in numeric_attr:
                plot_file_name = "{}_scatter".format(attribute_x)
                plot_title = "Scatter plot for {}".format(attribute_x)
                attribute_y = list(set(numeric_attr) - {attribute_x})
                self.scatter_plot.Join(self.dataset.data[attribute_x],
                                       self.dataset.data[attribute_y],
                                       file_name=plot_file_name,
                                       output_format=output_format,
                                       plot_title=plot_title, target=target)
        if report:
            ScatterReport(dataset=self.dataset).create_report(
                output_path=output_path,
                file_name=file_name)
